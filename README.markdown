## Ceph Kernel Client
- contribution by Dongsu Park from Oct 2007 to Apr 2008

## Overview

 Ceph <http://ceph.newdream.net> is a distributed network storage and file system designed to provide excellent performance, reliability, and scalability. Its file system client supports both the native Linux kernel module and FUSE driver on the userspace. This repository includes my contribution on the Ceph project, which was done from Oct. 2007 to Apr. 2008. This was one of the winter semester projects of Linux kernel lab, held by Distributed Systems Group (LuFGi4) at the RWTH Aachen University. At that time my job was to implement file system operations, porting the original software based on FUSE to the kernel module.

 Note that the code described here is not maintained any more from the main developers of Ceph. This repository is just for the summary of what I had achieved in the early phase of Ceph project.

## Implementation

### ceph_aops

```
const struct address_space_operations ceph_aops = {
	.readpage = ceph_readpage,
	.readpages = ceph_readpages,
	.prepare_write = ceph_prepare_write,
	.commit_write = ceph_commit_write,
	.writepage = ceph_writepage,
	.writepages = ceph_writepages,
};
```

### Codes

 The following codes are only the ones modified by me and other team.
 A brief description over the basic implementation is following.

 * addr.c : a set of implementations of address_space_operations
  - ceph_readpage, ceph_readpages : read one or more pages from remote OSDs, and update metadata.
  - ceph_writepage, ceph_writepages : write on or more pages to remote OSDs, and update metadata.
  - ceph_prepare_write, ceph_commit_write : make the page as dirty, so that it can be written to the disk later.
 * osd_client.c : implementation for the OSD client
  - subroutines called by address space operations described above.
 * osd_client.h : header for the OSD client
  - header of OSD client

## Git log

        commit 0cf82d05ef844cf2abef7df58b7881c92a827867
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Thu Mar 27 22:50:55 2008 +0100
        
            Added basic comments for OSD client functions
        
        commit 4bda344e9f436e56c52b2310bade433c04f1261e
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Thu Mar 27 20:21:01 2008 +0100
        
            added some comments
        
        commit 0738b3ab2a99f3566e60605a4dda8a79baeb9711
        Author: Volker Aßmann <volker.assmann@rwth-aachen.de>
        Date:   Mon Mar 24 15:51:06 2008 +0100
        
            Patch makefile to copy current ceph_fs.h to kernel dir for module only builds
        
        commit de7534cee55b23c974eae1ddea32ff43f8af78d0
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Tue Mar 18 20:29:45 2008 +0100
        
            enabled readpages
        
        commit dff9e66f44b1c70528579daf99db7dad01d49aad
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Wed Mar 5 09:57:45 2008 +0100
        
            defined ceph_osdc_readpages() and ceph_osdc_readpage_filler().
            
            renamed do_ceph_read as ceph_readpage_async(),
             do_ceph_write as ceph_writepage_async() respectively.
            
            now ceph_writepage method is really used,
            
            and buggy unlock_page() functions were removed, to avoid kernel panics.
        
        commit 933226e5cdea16d311f15d47d5db852cf2107e57
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Tue Feb 26 22:26:45 2008 +0100
        
            separated do_ceph_read() from ceph_osdc_read()
        
        commit 2d4092d66d1e33130a88c84d5692c2473bdc4b73
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Tue Feb 19 13:20:00 2008 +0100
        
            implemented do_sync_write()
        
        commit 4ce0c0cdc052e4d4b291c2a1bda3765404999d3b
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Tue Feb 5 12:20:49 2008 +0100
        
            implemented additional basic codes for prepare_write and commit_write
        
        commit 80df6af3ff236f207e2bf46823d2c60ed246e4f8
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Thu Jan 31 11:31:49 2008 +0100
        
            working copy implementing prepare_write and commit_write
        
        commit 02530a96bf557bb89579d5e9400af482bb195429
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Tue Jan 29 23:18:18 2008 +0100
        
            commit again prepare_write and commit_write skeletons
        
        commit bf03c27deed0c7b968cdd8275a5af96a1a4fb8b7
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Tue Jan 29 13:23:30 2008 +0100
        
            Defined ceph_prepare_write and ceph_commit_write.
            These are required for working with ceph_writepage!
        
        commit 04a54e63aa732ee12e69f13cd2083bc05f57296d
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Mon Jan 28 21:53:34 2008 +0100
        
            resolved a syntax error and typo in start.sh
        
        commit 9c1faddd3684ba080ccc4663a20982cef795a789
        Author: Volker Aßmann <volker.assmann@rwth-aachen.de>
        Date:   Mon Jan 28 02:19:01 2008 +0100
        
            Import changes from local rep and newdream rep
        
        commit 7f594140dc6855d5f005cf1e3fcc51b77bfb7ce9
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Wed Jan 23 17:05:38 2008 +0100
        
            added skeleton for some address_space_operations
            
            Added skeleton for prepare_write, commit_write and writepage in ceph_aops.
            Got src/kernel/Makefile used to build ceph kernel module by itself,
            so that you can compile sources without entering a real kernel src tree.
        
        commit 6c4dd5ad236225511af4d449fb01becb8b629a6e
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Wed Jan 23 16:02:20 2008 +0100
        
            Added missing files added as new files so far
        
        commit 63812e34b91de761a37f3f0e51b663c993499b34
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Wed Jan 23 14:48:01 2008 +0100
        
            Merge diffs from ceph.newdream.net, c370a7e...85af728
        
        commit 17c1bdaf3e693d3f1fbbac5e3925f4449ca48f6f
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Tue Jan 15 22:49:35 2008 +0100
        
            Added some ctags/cscope files in .gitignore
        
        commit 57cdee6accbc398e0f5afcfc823d8464b71c62cf
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Tue Jan 15 18:15:25 2008 +0100
        
            Added tags and cscope*out in .gitignore
        
        commit 3ee344cdebd17afb42541e3eb75d709625ed601c
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Tue Jan 8 14:22:57 2008 +0100
        
            remove 1234
        
        commit d8d46238a61db7e6111478d50878108cfd864183
        Author: volker <volker@36-135.mops.RWTH-Aachen.DE>
        Date:   Tue Jan 8 14:16:24 2008 +0100
        
            test
        
        commit f5c157a5939c968eadc73c6fc626355d3f42a666
        Author: volker <volker@stan.local>
        Date:   Sun Jan 6 23:42:30 2008 +0100
        
            Revert GIT test changes
        
        commit 67a834ef0c3f137315817011dc799ea367355360
        Author: volker <volker@stan.local>
        Date:   Sat Jan 5 01:11:43 2008 +0100
        
            GIT Test
        
        commit eaaebe5e0a4d893452985629ac40bd218fe6f29d
        Author: Dongsu Park <dpark1978@gmail.com>
        Date:   Sat Jan 5 00:59:51 2008 +0100
        
            Test for the public branch
        
        commit c370a7e2dfc60ae2e783907714b0eedade5badae
        Merge: 4642cc1 2ee1df1
        Author: Sage Weil <sage@newdream.net>
        Date:   Thu Jan 3 11:08:37 2008 -0800
        
            Merge branch 'rados' of ssh://ceph.newdream.net/home/sage/ceph.newdream.net/git/ceph into rados


## Etc

 * my documentation about the development : http://dongsupark.de/wiki/doku.php?id=linux:ceph
 * Authors
  - Dongsu Park advance38@gmail.com
  - Volker Assmann volker.assmann@rwth-aachen.de
  - Sage Weil sage@newdream.net

 * For the current status of the project, please refer to http://ceph.newdream.net.

