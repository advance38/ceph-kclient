#
# Makefile for CEPH filesystem.
#

ifneq ($(KERNELRELEASE),)

        obj-m := ceph.o

	ceph-objs := super.o inode.o dir.o file.o addr.o \
	ktcp.o messenger.o \
	client.o \
	mds_client.o mdsmap.o \
	mon_client.o \
	osd_client.o osdmap.o crush/crush.o crush/mapper.o
#Otherwise we were called directly from the command
# line; invoke the kernel build system.
else

         KERNELDIR ?= /lib/modules/$(shell uname -r)/build
         PWD := $(shell pwd)

default:
ifeq ($(KERNELRELEASE),)
	cp ../include/ceph_fs.h /lib/modules/$(shell uname -r)/build/include/linux/
endif
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules

endif
